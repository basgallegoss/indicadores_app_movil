import axios from "axios";
import { useQuery } from "react-query";

export const getDetalle = async (dataFiltro) => {
  const [queryName, paramsFilter] = dataFiltro?.queryKey;
  const { data } = await axios.get(
    "https://mindicador.cl/api/"+`${paramsFilter}`
  );
  return data?.serie;
};

export function useDetalle(dataFiltro) {
  return useQuery(["detalleTipo", dataFiltro], getDetalle, {
    retry: 1,
    keepPreviousData: true,
    refetchOnWindowFocus: false,
  });
}
