import axios from "axios";
import { useQuery } from "react-query";

export const getIndicadores = async () => {
  const { data } = await axios.get(
    "https://mindicador.cl/api"
  );

  return [data];
};

export function useIndicadores() {
  return useQuery(["indicadores"], getIndicadores, {
    retry: 1,
    keepPreviousData: true,
    refetchOnWindowFocus: false,
  });
}
