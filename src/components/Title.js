import React, { memo } from 'react';
import { StyleSheet, Text } from 'react-native';

const Title = ({ children }) =>
  <Text
    fontSize="lg"
    fontWeight="bold"
    textTransform="uppercase"
    color="gray"
    letterSpacing={2}
    textAlign="justify"
  > {children}</Text>;

export default memo(Title);
