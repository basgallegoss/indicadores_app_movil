import React, { memo } from 'react';
import { Div, Skeleton } from 'react-native-magnus';

const ListLoading = () => (
  <>
    <Div flexDir="row" mt="md">
      <Div ml="md" flex={1}>
        <Skeleton.Box mt="sm"  h={60} alignSelf='center' w={'90%'}/>
      </Div>
    </Div>
    <Div flexDir="row" mt="md">
      <Div ml="md" flex={1}>
        <Skeleton.Box mt="sm"  h={60} alignSelf='center' w={'90%'}/>
      </Div>
    </Div>
    <Div flexDir="row" mt="md">
      <Div ml="md" flex={1}>
        <Skeleton.Box mt="sm"  h={60} alignSelf='center' w={'90%'}/>
      </Div>
    </Div>
    <Div flexDir="row" mt="md">
      <Div ml="md" flex={1}>
        <Skeleton.Box mt="sm"  h={60} alignSelf='center' w={'90%'}/>
      </Div>
    </Div>
    <Div flexDir="row" mt="md">
      <Div ml="md" flex={1}>
        <Skeleton.Box mt="sm"  h={60} alignSelf='center' w={'90%'}/>
      </Div>
    </Div>
    <Div flexDir="row" mt="md">
      <Div ml="md" flex={1}>
        <Skeleton.Box mt="sm"  h={60} alignSelf='center' w={'90%'}/>
      </Div>
    </Div>
    <Div flexDir="row" mt="md">
      <Div ml="md" flex={1}>
        <Skeleton.Box mt="sm"  h={60} alignSelf='center' w={'90%'}/>
      </Div>
    </Div>
    <Div flexDir="row" mt="md">
      <Div ml="md" flex={1}>
        <Skeleton.Box mt="sm"  h={60} alignSelf='center' w={'90%'}/>
      </Div>
    </Div>
    <Div flexDir="row" mt="md">
      <Div ml="md" flex={1}>
        <Skeleton.Box mt="sm"  h={60} alignSelf='center' w={'90%'}/>
      </Div>
    </Div>
    <Div flexDir="row" mt="md">
      <Div ml="md" flex={1}>
        <Skeleton.Box mt="sm"  h={60} alignSelf='center' w={'90%'}/>
      </Div>
    </Div>
  </>
);

export default memo(ListLoading);
