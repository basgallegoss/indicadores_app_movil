const theme = {
    colors: {
      blue800: '#00728d',
      blue700: '#2b6cb0',
      blue600: '#3182ce',
      red600: '#e53e3e',
    },
    shadows: {
      '3xl': {
        shadowOffset: {
          width: 0,
          height: 10,
        },
        shadowOpacity: 0.8,
        shadowRadius: 14,
        elevation: 25,
      },
    },
    fontSize: {
      xs: 10,
      '7xl': 64,
    },
  };
  
  export default theme;
  