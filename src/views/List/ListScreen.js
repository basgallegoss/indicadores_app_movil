import React, { memo, useState, useEffect } from 'react';
import { FlatList, PermissionsAndroid } from "react-native";
import Header from "../../components/HeaderBar";
import ListLoading from "../../components/Loading/ListLoading";
import ButtonList from "../../components/ButtonList";
import { useIndicadores } from '../../services/getIndicadores';
import ModalLocalizacion from "../../components/ModalLocalizacion";
const ListScreen = ({ navigation }) => {
    const [permisoLocalizacion, setPermisoLocalizacion] = useState(false);
    useEffect(() => {
        const interval = setInterval(() => {
            permisos();
          }, 2000);
          return () => clearInterval(interval);
    }, []);

    const permisos = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
            },
            );
            switch (granted) {
                case "granted":
                    try {
                        setPermisoLocalizacion(false)
                    } catch (err) {
                        console.log('error granted geolocalización', err);
                    }
                    break;
                case "never_ask_again":
                    try {
                        setPermisoLocalizacion(true)
                    } catch (err) {
                        console.log('error never_ask_again geolocalización', err);
                    }
                    break;
                case "denied":
                    try {
                        setPermisoLocalizacion(true)
                    } catch (err) {
                        console.log('error denied geolocalización', err);
                    }
                    break;
            }
        } catch (err) {
            Warning(err);
        }
        return null;
    }

    const {
        data,
        isLoading,
        isError,
    } = useIndicadores();

    if (isLoading) {
        return (
            <ListLoading />
        );
    }

    if (isError) {
        console.log('ERROR!!!')
    }

    return (
        <>
            <Header
                children={'Indicadores'} />
            <FlatList
                data={data}
                style={{ height: '90%' }}
                keyExtractor={(item, index) => 'key' + index}
                renderItem={({ item, index }) => {
                    return (
                        <>
                            <ButtonList
                                navigation={navigation}
                                item={item?.bitcoin}
                            />
                            <ButtonList
                                navigation={navigation}
                                item={item?.dolar}
                            />
                            <ButtonList
                                navigation={navigation}
                                item={item?.dolar_intercambio}
                            />
                            <ButtonList
                                navigation={navigation}
                                item={item?.euro}
                            />
                            <ButtonList
                                navigation={navigation}
                                item={item?.imacec}
                            />
                            <ButtonList
                                navigation={navigation}
                                item={item?.ipc}
                            />
                            <ButtonList
                                navigation={navigation}
                                item={item?.ivp}
                            />
                            <ButtonList
                                navigation={navigation}
                                item={item?.libra_cobre}
                            />
                            <ButtonList
                                navigation={navigation}
                                item={item?.tasa_desempleo}
                            />
                            <ButtonList
                                navigation={navigation}
                                item={item?.tpm}
                            />
                            <ButtonList
                                navigation={navigation}
                                item={item?.uf}
                            />
                            <ButtonList
                                navigation={navigation}
                                item={item?.utm}
                            />
                        </>
                    )
                }}
                onEndReachedThreshold={0.1}
            />
            <ModalLocalizacion permisoLocalizacion={permisoLocalizacion} />
        </>
    );
};


export default memo(ListScreen);
