import React from 'react';
import { Button, Div, Text, Icon, Modal } from 'react-native-magnus';
import { SafeAreaView } from 'react-native';

const ModalLocalizacion = ({ permisoLocalizacion }) => {
    return (
        <Div alignItems={'center'}>
            <SafeAreaView >
                <Modal isVisible={permisoLocalizacion} bg={'#0e056e'}>
                    <Div top={'20%'}>
                        <Icon color="white" name="location" fontFamily="EvilIcons"  fontSize={70}/>
                    </Div>
                    <Div top={'30%'}>
                        <Text color={'white'} textAlign={'center'} fontSize="3xl">Lo sentimos para que esto funcione necesitamos que tenga permisos de localización.</Text>
                    </Div>
                </Modal>
            </SafeAreaView>
        </Div>
    )
}

export default (ModalLocalizacion);
