import React, { memo, useEffect, useState } from 'react';
import { View, StyleSheet, StatusBar } from "react-native";
import { Button, Icon, Div, Text } from 'react-native-magnus';
import Portada from "../../components/Portada";
import ModalInternet from "../../components/ModalInternet";
import NetInfo from "@react-native-community/netinfo";
const Login = ({ navigation }) => {
  const [net, setNet] = useState(false);
  const internet = (data) => {
    
    NetInfo.fetch().then(state => {
      if (!state.isConnected) {
        setNet(true)
      } else {
        setNet(false)
      }
    });
  }

  useEffect(() => {
    const interval = setInterval(() => {
      internet();
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <>
      <View style={styles.background}>
        <StatusBar backgroundColor="#0e056e" />
        <View style={styles.logo}>
          <Portada />
        </View>
        <View></View>
        <Div bg={'#0e056e'} h={'100%'} mt={'-30%'} roundedTop={100} shadow="2xl">
          <Text
            fontSize="lg"
            fontWeight="bold"
            textTransform="uppercase"
            color="white"
            letterSpacing={2}
            mt={'20%'}
            textAlign="center"
          > Indicador App</Text>
          <Button
            mt={'30%'}
            py="lg"
            bg="#0e056e"
            alignSelf="center"
            borderWidth={1}
            borderColor="white"
            color="white"
            shadow={100}
            underlayColor="red100"
            onPress={() => navigation.navigate('ListScreen')}
            suffix={<Icon name="arrowright" ml="md" color="white" />}
          >Ingresar</Button>
        </Div>
      </View>
      <ModalInternet net={net} />
    </>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    height: '100%',
    width: '100%',
    backgroundColor: 'white'
  },
  logo: {
    marginTop: '4%'
  },
  boton: {
    width: '40%',
    marginTop: '5%',
    alignSelf: 'center',
    color: 'red',
  }
});
export default memo(Login);
