import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../views/Login/LoginScreen';
import ListScreen from '../views/List/ListScreen';
import RecordScreen from '../views/Record/RecordScreen';
import RecordMoneyScreen from '../views/Record/RecordMoneyScreen';
const PrincipalStack = createStackNavigator();

const MainStack = () => {
  return (
    <>
      <PrincipalStack.Navigator  >
        <PrincipalStack.Screen
          name="LoginScreen"
          component={LoginScreen}
          options={{ headerShown: false }}
        />
        <PrincipalStack.Screen
          name="ListScreen"
          component={ListScreen}
          options={{ headerShown: false }}
        />
        <PrincipalStack.Screen
          name="RecordScreen"
          component={RecordScreen}
          options={{ headerShown: false }}
        />
        <PrincipalStack.Screen
          name="RecordMoneyScreen"
          component={RecordMoneyScreen}
          options={{ headerShown: false }}
        />
      </PrincipalStack.Navigator>
    </>
  );
};
export default MainStack;
