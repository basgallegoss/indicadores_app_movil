import React from 'react';
import {useState} from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { NavigationContainer } from '@react-navigation/native';
import { navigationRef } from './src/stacks/RootNavigator';
import DrawerContent from './src/layouts/Drawer/DrawerContent';
import { QueryClient, QueryClientProvider } from "react-query";
import { MainStack } from './src/stacks/index';
import { theme } from './src/theme/theme';
import { ThemeProvider } from 'react-native-magnus';
const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();
const queryClient = new QueryClient();

const App = () => {


  return (
    <QueryClientProvider client={queryClient}>
      <NavigationContainer ref={navigationRef}>
        <ThemeProvider theme={theme}>
          <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
            <Stack.Screen hide name="Principal" options={{ headerShown: false }} component={MainStack} />
          </Drawer.Navigator>
        </ThemeProvider>
      </NavigationContainer>
    </QueryClientProvider>
  )
}

export default App;
