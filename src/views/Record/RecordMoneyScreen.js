import React, { memo, useState } from 'react';
import HeaderBarD from "../../components/HeaderBarD";
import ListLoading from "../../components/Loading/ListLoading";
import { Div, Text } from 'react-native-magnus';
import {
    LineChart,
} from "react-native-chart-kit";
import { useDetalle } from '../../services/getDetalle';
import { Dimensions } from "react-native";
const screenWidth = Dimensions.get("window").width;
const RecordMoneyScreen = ({ route, navigation }) => {
    const { items } = route.params;
    const {
        data,
        isLoading,
        isError,
    } = useDetalle(items?.codigo);

    if (isLoading) {
        return (
            <ListLoading />
        );
    }

    if (isError) {
        console.log('ERROR!!!')
    }

    return (
        <Div>
            <HeaderBarD
                children={items?.nombre} />
            <Div bg={'#0e056e'} w={'100%'} h={'50%'} mt={'-1%'} shadow="lg" shadowColor="#0e056e" roundedBottom="xl" >
                <Div mt={'10%'}>
                    <Text
                        fontSize={50}
                        fontWeight="bold"
                        textTransform="uppercase"
                        color="white"
                        letterSpacing={2}
                        textAlign="center"
                    > ${items?.valor}</Text>
                </Div>
                <Div mt={'10%'}>
                    <Text
                        fontSize={15}
                        fontWeight="bold"
                        textTransform="uppercase"
                        color="white"
                        letterSpacing={2}
                        textAlign="center"
                    > Nombre: {items?.nombre}</Text>
                    <Text
                        fontSize={15}
                        fontWeight="bold"
                        textTransform="uppercase"
                        color="white"
                        letterSpacing={2}
                        textAlign="center"
                    > Fecha: {items?.fecha.substr(0, 10)}</Text>
                    <Text
                        fontSize={15}
                        fontWeight="bold"
                        textTransform="uppercase"
                        color="white"
                        letterSpacing={2}
                        textAlign="center"
                    > Unidad de Medida: {items?.unidad_medida}</Text>
                </Div>
            </Div>
            <Div alignSelf='center'>
                <LineChart

                    data={{
                        labels: [
                            data[0]?.fecha.substr(0, 10),
                            data[3]?.fecha.substr(0, 10),
                            data[5]?.fecha.substr(0, 10),
                            data[9]?.fecha.substr(0, 10),
                        ],
                        datasets: [
                            {
                                data: [
                                    data[0]?.valor,
                                    data[1]?.valor,
                                    data[2]?.valor,
                                    data[3]?.valor,
                                    data[4]?.valor,
                                    data[5]?.valor,
                                    data[6]?.valor,
                                    data[7]?.valor,
                                    data[8]?.valor,
                                    data[9]?.valor,
                                ]
                            }
                        ]
                    }}
                    width={Dimensions.get("window").width - 10}
                    height={220}
                    yAxisLabel="$"
                    yAxisSuffix=""
                    yAxisInterval={1} // optional, defaults to 1
                    chartConfig={{
                        backgroundColor: "#0e056e",
                        backgroundGradientFrom: "#0e056e",
                        backgroundGradientTo: "#0e056e",
                        decimalPlaces: 2, // optional, defaults to 2dp
                        color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        style: {
                            borderRadius: 0
                        },
                        propsForDots: {
                            r: "6",
                            strokeWidth: "2",
                            stroke: "#ffa726"
                        }
                    }}
                    bezier
                    style={{
                        marginVertical: 8,
                        borderRadius: 16
                    }}
                />
            </Div>
        </Div>
    );
};


export default memo(RecordMoneyScreen);
