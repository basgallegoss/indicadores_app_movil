import React from 'react';
import { Button, Div, Text, Icon, Modal } from 'react-native-magnus';
import { SafeAreaView } from 'react-native';

const ModalInternet = ({ net }) => {
    return (
        <Div alignItems={'center'}>
            <SafeAreaView >
                <Modal isVisible={net} bg={'#0e056e'}>
                    <Div top={'20%'}>
                    <Icon color="white" name="wifi-off" fontFamily="Feather"  fontSize={70}/>
                    </Div>
                    <Div top={'30%'}>
                        <Text color={'white'} textAlign={'center'} fontSize="3xl">Lo sentimos para que esto funcione necesitamos que tenga conexión a internet.</Text>
                    </Div>
                </Modal>
            </SafeAreaView>
        </Div>
    )
}

export default (ModalInternet);
