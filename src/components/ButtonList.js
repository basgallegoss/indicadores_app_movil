import React, { memo } from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Button, Icon, Div, Text } from 'react-native-magnus';

const ButtonList = ({ navigation, item }) =>
  
    <Div flexDir="row" h={60} alignSelf='center' w={'90%'} mt="md" mb={5} bg={'white'} rounded="md" shadow="md" borderBottomColor={'gray'} underlayColor="white">

      <Div bg="#0e056e" w={"5%"} roundedTopLeft="md" roundedBottomLeft="md">
      </Div>
      <Div alignItems='flex-start' alignSelf='center' w="60%" mb={5} ml={20} >
        <Text
          fontSize="md"
          fontWeight="bold"
          textTransform="uppercase"
          color="black"
          letterSpacing={1}
          textAlign="justify"
        > {item?.nombre}</Text>
        <Text
          fontSize="sm"
          fontWeight="bold"
          textTransform="uppercase"
          color="gray"
          letterSpacing={2}
          textAlign="justify"
        > {item?.unidad_medida}</Text>
      </Div>
      <Button bg="white" h={46} w={'14%'} mr={10} alignSelf="center" onPress={() => navigation.navigate('RecordMoneyScreen', { items: item })}>
        <Icon name="infocirlceo" color="gray" fontSize={22} />
      </Button>
      <Button bg="white" h={45} w={'12%'} mr={10} alignSelf="center" onPress={() => navigation.navigate('RecordScreen', { items: item })} >
        <Icon name="right" color="gray" fontSize={22} />
      </Button>

    </Div>;

export default memo(ButtonList);
