import React, { memo, useState } from 'react';
import { FlatList } from "react-native";
import Header from "../../components/HeaderBar";
import ListLoading from "../../components/Loading/ListLoading";
import { Button, Icon, Div, Text } from 'react-native-magnus';
import { useDetalle } from '../../services/getDetalle';
const RecordScreen = ({ route, navigation }) => {
    const { items } = route.params;
    const {
        data,
        isLoading,
        isError,
    } = useDetalle(items?.codigo);

    if (isLoading) {
        return (
            <ListLoading />
        );
    }

    if (isError) {
        console.log('ERROR!!!')
    }

    return (
        <Div>
            <Header
                children={items?.nombre} />
            <FlatList
                data={data}
                style={{ marginTop:'1%',height: '89%' }}
                keyExtractor={(item, index) => 'key' + index}
                renderItem={({ item, index }) => {
                    return (
                        <>
                            <Div flexDir="row" h={50} alignSelf='center' w={'90%'} mt="md" bg={'white'} rounded="md" shadow="md" borderBottomColor={'gray'} underlayColor="white">
                                <Div bg="#0e056e" w={"5%"} roundedTopLeft="md" roundedBottomLeft="md">
                                </Div>
                                <Div  row alignSelf='center' w="60%" mb={5} ml={20} >
                                    <Text
                                        fontSize="lg"
                                        fontWeight="bold"
                                        textTransform="uppercase"
                                        color="black"
                                        letterSpacing={2}
                                        textAlign="justify"
                                        mr={'40%'}
                                    > {item?.fecha.substr(0,10)}</Text>
                                    <Text
                                        fontSize="sm"
                                        fontWeight="bold"
                                        textTransform="uppercase"
                                        color="gray"
                                        letterSpacing={2}
                                        textAlign="justify"
                                    > ${item?.valor}</Text>
                                </Div>
                                
                            </Div>
                        </>
                    )
                }}
                onEndReachedThreshold={0.1}
            />
        </Div>
    );
};


export default memo(RecordScreen);
