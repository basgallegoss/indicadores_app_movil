import * as React from 'react';
import { Header, Icon, Button } from 'react-native-magnus';
import { StatusBar } from "react-native";
import { useNavigation } from '@react-navigation/native';
const HeaderBar = ({ children}) => {
    const navigation = useNavigation();
    return (
        <>
            <StatusBar backgroundColor="#0e056e"  />
            <Header
                p="lg"
                borderBottomWidth={1}
                bg={'#0e056e'}
                roundedBottom="xl" shadow="lg"
                borderBottomColor="gray200"
                alignment="center"
                color="white"
                prefix={
                    <Button bg="transparent" onPress={() => navigation.goBack()}>
                        <Icon name="arrow-left" fontFamily="Feather" fontSize="2xl" color="white" />
                    </Button>
                }
            >
                {children}
            </Header>
        </>
    );
};

export default HeaderBar;