## Descripción

- Proyecto indicadores economicos diarios, utilizando API https://mindicador.cl/. 

- Unidad de fomento (UF):	
- Valores desde 1977 hasta hoy.
- Libra de Cobre:	Valores desde 2012 hasta hoy.
- Tasa de desempleo:	Valores desde 2009 hasta hoy.
- Euro:	Valores desde 1999 hasta hoy.
- Imacec:	Valores desde 1997 hasta hoy.
- Dólar observado:	Valores desde 1984 hasta hoy.
- Tasa Política Monetaria (TPM):	Valores desde 2001 hasta hoy.
- Indice de valor promedio (IVP):	Valores desde 1990 hasta hoy.
- Indice de Precios al Consumidor (IPC):	Valores desde 1928 hasta hoy.
- Dólar acuerdo:	Valores desde 1988 hasta hoy.
- Unidad Tributaria Mensual (UTM):	Valores desde 1990 hasta hoy.
- Bitcoin: 	Valores desde 2009 hasta hoy.


## Instalación

- clonar repo.
- instalar las dependencias `yarn`.
- instalar proyecto celular `yarn run android`.
- ejecutar metro `yarn start`.

# Autor
Creado por Bastián Gallegos Sepúlveda.




