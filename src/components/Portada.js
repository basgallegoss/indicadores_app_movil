import React, { memo } from 'react';
import { Image, StyleSheet } from 'react-native';

const Portada = () => (
  <Image source={require('../img/20943809.jpg')} style={styles.image} />
);

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '65%',
    alignSelf: 'center'
  },
});
export default Portada;

